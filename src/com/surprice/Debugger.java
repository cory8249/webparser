package com.surprice;

import java.io.*;

class Config {
    public enum Platform{PLATFORM_PC, PLATFORM_ANDROID}

    public static final String STORAGE_PATH = "./";
    // public static final String STORAGE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";


    public static final String LOG_FILENAME =  "/debug_msg.txt";

    public static boolean debuggerPrintEnabled = true;
    public static boolean debuggerLogFileEnabled = true;
}

public class Debugger {

    private static File mLogFile;
    private static PrintWriter printWriter;
    private static final String M_CHARSET = "UTF-8";

    static {
        try {
            mLogFile = new File(Config.STORAGE_PATH + Config.LOG_FILENAME);
            printWriter = new PrintWriter(mLogFile, M_CHARSET);
        } catch (final IOException e) {
            throw new ExceptionInInitializerError(e.getMessage());
        }
    }

    public static void print(Object msg, String... tags){
        if(!Config.debuggerPrintEnabled)
            return;

        for(String tag : tags){
            System.out.print("[" + tag + "]");
        }
        if(tags.length != 0){
            System.out.print("  ");
        }
        System.out.print(msg.toString());
    }

    public static void println(Object msg, String... tags){
        if(!Config.debuggerPrintEnabled)
            return;
        print(msg.toString() + "\n", tags);
    }

    public static void printToFile(Object msg){
        if(!Config.debuggerLogFileEnabled)
            return;
        printWriter.print(msg.toString());
    }

    public static void printlnToFile(Object msg){
        if(!Config.debuggerLogFileEnabled)
            return;
        printWriter.print(msg.toString() + "\n");
    }

    public static void closeFile(){
        if(!Config.debuggerLogFileEnabled)
            return;
        printWriter.close();
    }
}
