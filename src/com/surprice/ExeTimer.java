package com.surprice;

public class ExeTimer {
    private long startTime = 0;
    private long endTime = 0;
    private long durationMs = 0;
    private String label;

    public void start(String label){
        this.label = label;
        startTime = System.nanoTime();
    }

    public void stop(){
        endTime = System.nanoTime();
        durationMs = (endTime - startTime) / 1_000_000;  //divide by 1000000 to get milliseconds.
        Debugger.println(label + " takes " + String.valueOf(durationMs) + " ms", "ExeTimer");
    }

    public long getDurationMs(){
        return durationMs;
    }
}
