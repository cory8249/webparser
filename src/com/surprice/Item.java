package com.surprice;

public class Item implements Comparable<Item>{

    private String text;
    private int price;
    private String url;
    private Store store;
    private String imgUrl;

    public Item(){
        text = "";
        price = 0;
        url = "";
        store = Store.NONE;
        imgUrl = "";
    }

    public Item(String name, int price, String url, Store store, String imgUrl){
        this.text = name;
        this.price = price;
        this.url = url;
        this.store = store;
        this.imgUrl = imgUrl;
    }

    public int getPrice() {
        return price;
    }

    public String getText(){
        return text;
    }

    public String getUrl(){
        return url;
    }

    public Store getStore(){
        return store;
    }

    public String getImgUrl(){
        return imgUrl;
    }

    @Override
    public String toString(){
        if(text.equals(""))
            return "Empty Item";
        String str = "$ " + String.valueOf(price) + " [" + store +  "] \"" + text + "\" " + url;
        return str;
    }

    @Override
    public int compareTo(Item other){
        if(this.price == other.price){
            return this.text.compareTo(other.text);
        }
        return this.price - other.price;
    }
}
