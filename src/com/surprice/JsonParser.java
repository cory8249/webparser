package com.surprice;

import com.google.gson.Gson;
import com.sun.org.apache.xml.internal.resolver.helpers.Debug;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

class Wrapper {
    String p;
    String pc;
    int sp;
}

public class JsonParser {

    public static void main(String[] args) {

        List itemList = new ArrayList<Item>();
        try {
            WebThread webThread = new WebThread("http://www.isunfar.com.tw/ecdiy/getitem.ashx", "UTF-8");
            webThread.useCache(true);
            webThread.setCacheExpireTimeMs(60 * 60 * 1000L); // expiration time: 1 hour
            webThread.start();
            webThread.join();
            webThread.dumpToFile(Config.STORAGE_PATH + "sunfar.ashx");

            String fileText;
            BufferedReader br = new BufferedReader(new FileReader("sunfar.ashx"));
            try {
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();

                while (line != null) {
                    sb.append(line);
                    sb.append("\n");
                    line = br.readLine();
                }
                fileText = sb.toString();
            } finally {
                br.close();
            }

            // Change ashx to json string
            String beginStr = "{\"ed\":[";
            String endStr = "],";
            int jsonBeginIndex =  fileText.indexOf(beginStr);
            int jsonEndIndex = fileText.indexOf(endStr);
            fileText = fileText.substring(jsonBeginIndex + beginStr.length() - 1, jsonEndIndex + 1);

            // parse items
            Gson gson = new Gson();
            Wrapper[] arr = gson.fromJson(fileText, Wrapper[].class);
            for (Wrapper wp : arr) {
                int price = wp.sp;
                String name = wp.pc.substring(0, wp.pc.indexOf("＄"));
                String url = "http://www.isunfar.com.tw/product/?prodseq=" + wp.p + "#/bcNo=16&ept=58";
                String imgUrl = "http://images.sunfar.com.tw/jpg360/" + wp.p.substring(0,3) + "/" + wp.p + "YF10.jpg";
                Item item = new Item(name, price, url, Store.SUNFAR, imgUrl);
                itemList.add(item);
            }

            Debugger.println("itemList.size() = " + itemList.size(), "Sunfar");
            JsonWriter.output("sunfar", itemList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
