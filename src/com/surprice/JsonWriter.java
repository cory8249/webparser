package com.surprice;


import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class JsonWriter {
    public static void output(String className, List<Item> itemList){
        try {
            PrintWriter json_writer = new PrintWriter(className + ".json", "UTF-8");
            json_writer.println("{\"results\":[");
            Iterator iter = itemList.iterator();
            while (iter.hasNext()) {
                Item item = (Item) iter.next();
                json_writer.print("{\"name\":\"" + item.getText().replace("\"", "\\\"") + "\", ");
                json_writer.print("\"price\":" + item.getPrice() + ", ");
                json_writer.print("\"url\":\"" + item.getUrl() + "\", ");
                json_writer.print("\"imgUrl\":\"" + item.getImgUrl() + "\", ");

                ArrayList<String> keywords = new ArrayList<>(KeywordSplitter.splitToKeywords(item.getText()));
                json_writer.print("\"keywords\":\"");
                for(String str : keywords){
                    json_writer.print(str + " ");
                }
                json_writer.print("\"}");
                if (iter.hasNext()) {
                    json_writer.println(",");
                }
            }
            json_writer.println("]}");
            json_writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
