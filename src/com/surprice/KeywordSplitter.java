package com.surprice;
import java.util.*;

public class KeywordSplitter {

    enum CharType {ENG, NUM, CHINESE, SYM;

        @Override
        public String toString(){
            switch(this){
                case ENG: return "A";
                case NUM: return "0";
                case CHINESE: return "C";
                case SYM: return "_";
            }
            return "z";
        }
    }

    public static Set<String> splitToKeywords(String text){

        if(text == null || text.isEmpty()){
            return new HashSet<>();
        }

        CharType[] charArray = analysisCharType(text);
        Set<String> keywordSet = new HashSet<>();
        List<Integer> splitPoints = new ArrayList<>();

        // split points
        splitPoints.add(0);
        for(int i=0; i<charArray.length-1; i++){
            if(charArray[i] != charArray[i+1]){
                splitPoints.add(i+1);
            }
        }
        splitPoints.add(charArray.length);

        String bufStr = null;
        for(int i=0; i<splitPoints.size()-1; i++){
            int begin = splitPoints.get(i);
            int end = splitPoints.get(i + 1);
            String str = text.substring(begin, end);

            // determine whether to split at this point
            if(charArray[begin] == CharType.SYM){
                if(bufStr != null) {
                    keywordSet.add(bufStr.toLowerCase(Locale.ENGLISH));
                    bufStr = null;
                }
                continue;
            }else if((charArray[begin] == CharType.ENG  && str.length() <= 2)
                    || (charArray[begin] == CharType.NUM && str.length() <= 4)
                    ){
                // detect short eng & num, store to buffer
                if(bufStr != null){
                    if(bufStr.length() <= 5){
                        bufStr += str;
                    }else{
                        keywordSet.add(bufStr.toLowerCase(Locale.ENGLISH));
                        bufStr = str;
                    }
                }else {
                    bufStr = str;
                }
                continue;
            }

            // normal case
            keywordSet.add(str.toLowerCase(Locale.ENGLISH));

            if(bufStr != null) {
                keywordSet.add(bufStr.toLowerCase(Locale.ENGLISH));
                bufStr = null;
            }
            //System.out.println(end + ":" + str);
        }
        if(bufStr != null) {
            keywordSet.add(bufStr.toLowerCase(Locale.ENGLISH));
        }

        Set<String> uniSet = unifyBrand(keywordSet);

        return uniSet;
    }

    public static Set<String> unifyBrand(Set<String> set){
        Iterator<String> iterator = set.iterator();

        Map<String, String> brandAlias = new HashMap<>();
        brandAlias.put("華碩", "asus");
        brandAlias.put("技嘉", "gigabyte");
        brandAlias.put("微星", "msi");
        brandAlias.put("華擎", "asrock");
        brandAlias.put("小米", "xiaomi");
        brandAlias.put("三星", "samsung");
        brandAlias.put("華為", "huawei");
        brandAlias.put("樂金", "lg");
        brandAlias.put("美光", "micron");
        brandAlias.put("威剛", "adata");
        brandAlias.put("羅技", "logitech");
        brandAlias.put("微軟", "microsoft");

        Set<String> uniSet = new HashSet<>();

        while (iterator.hasNext()) {
            String element = iterator.next();
            if (brandAlias.containsKey(element)) {
                uniSet.add(brandAlias.get(element));
            }else{
                uniSet.add(element);
            }
        }

        return uniSet;
    }

    public static Set<String> matchSet(String text, Collection<String> patterns){
        Set<String> set = new HashSet<>();
        for(String p : patterns){
            if(text.contains(p)){
                set.add(p);
            }
        }
        return set;
    }

    // determine character type (english, number, chinese, symbols)
    public static CharType charType(Character c){
        CharType type;
        int cp = c;
        if(c >= '0' && c<= '9'){
            type = CharType.NUM;
        }else if(c >= 'A' && c <= 'Z'){
            type = CharType.ENG;
        }else if(c >= 'a' && c <= 'z'){
            type = CharType.ENG;
        }else if (cp > 0x4E00 && cp < 0x9FCC) { // Chinese character unicode range
            type = CharType.CHINESE;
        }else if(cp == '+' || cp == '.'){
            type = CharType.ENG; // set '+' & '.' as a character
        }else{
            type = CharType.SYM;
        }
        return type;
    }
    private static CharType[] analysisCharType(String str){
        CharType[] cType = new CharType[str.length()];
        for(int i=0; i<str.length(); i++){
            char c = str.charAt(i);
            cType[i] = charType(c);
        }
        return cType;
    }

    public static void test(String str){
        System.out.println(KeywordSplitter.splitToKeywords(str));
    }

    public static void testCharType(String str){
        CharType[] types = analysisCharType(str);
        for(CharType ct : types){
            System.out.print(ct);
        }
        System.out.println();
        test(str);
    }

    public static void main(String [] args){
        test("i7-4790k");
        test("5200U");
        test("htc m9+");
        test("gtx750ti");
        test("gtx750");
        test("UX305FA");
        test("zenfone2");
        test("iphone6s");
        test("note5");
        test("華碩 B150M-K D3(M-ATX/1A1D/U3S6/全固/四年到府)");
        testCharType("華擎 Beebox N3000(lntel N3000-白/4G/128G SSD)【送羅技M165無線滑鼠】");
        testCharType("Razer 戰車世界Deathadder煉獄奎蛇光學滑鼠/有線/6400dpi【客訂】");
    }
}
