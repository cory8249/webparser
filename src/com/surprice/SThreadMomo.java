package com.surprice;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Set;

public class SThreadMomo extends Thread{

    private String mQuery;
    private Set<Item> mItemSet = new HashSet<>();
    private ExeTimer exeTimer = new ExeTimer();
    private final String M_CHARSET = "UTF-8";
    private int pLowerBound = 0;
    private int pUpperBound = Integer.MAX_VALUE;

    public SThreadMomo(String query){
        mQuery = query;
    }

    @Override
    public void run(){
        momoSearch();
    }

    public void setPriceRange(int lowerBound, int upperBound){
        pLowerBound = lowerBound;
        pUpperBound = upperBound;
    }

    public Set<Item> getItemSet(){
        return mItemSet;
    }

    // search ruten website
    private void momoSearch(){
        String queryStrEnc = "";
        try{
            queryStrEnc = URLEncoder.encode(mQuery, M_CHARSET);
        }catch (Exception e){
        }
        String searchUrl = "http://m.momoshop.com.tw/mosearch/" + queryStrEnc + ".html";

        try {
            exeTimer.start("momo download web");
            WebThread webThread = new WebThread(searchUrl, M_CHARSET);
            webThread.start();
            webThread.join();
            webThread.dumpToFile("debug_momo.html");
            Document doc = webThread.getDoc();
            exeTimer.stop();

            exeTimer.start("ruten parse web ");
            Elements elements = doc.select("div.directoryPrdListArea").select("ul").select("li");
            exeTimer.stop();
            Debugger.println("downloadSizeByte = " + webThread.htmlSize()/1024 + " KB", "Ruten");
            Debugger.println("elementsCount = " + elements.size(), "Ruten");

            exeTimer.start("ruten match items");
            for (Element elem : elements) {

                String itemText = elem.select("p.prdName").text();
                int itemPrice = Integer.parseInt(elem.select("b.price").text().replace(",",""));

                String baseUrl = "http://m.momoshop.com.tw/";
                String url = baseUrl + elem.child(1).attr("href");
                String imgUrl = elem.child(1).child(0).attr("org");

                Item item = new Item(itemText, itemPrice, url, Store.MOMO, imgUrl);

                mItemSet.add(item);
                Debugger.println(item);
            }
            exeTimer.stop();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String [] args){
        SThreadMomo thd = new SThreadMomo("zenfone 2");
        thd.run();

        try{
            thd.join();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
