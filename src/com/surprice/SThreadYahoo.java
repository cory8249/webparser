package com.surprice;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Set;

public class SThreadYahoo extends Thread {

    private String mQuery;
    private Set<Item> mItemSet = new HashSet<>();
    private ExeTimer exeTimer = new ExeTimer();
    private final String M_CHARSET = "UTF-8";
    private int pLowerBound = 0;
    private int pUpperBound = Integer.MAX_VALUE;

    public SThreadYahoo(String query) {
        mQuery = query;
    }

    @Override
    public void run() {
        yahooSearch();
    }

    public void setPriceRange(int lowerBound, int upperBound) {
        pLowerBound = lowerBound;
        pUpperBound = upperBound;
    }

    public Set<Item> getItemSet() {
        return mItemSet;
    }

    // search yahoo website
    private void yahooSearch() {
        String queryStrEnc = "";
        try {
            queryStrEnc = URLEncoder.encode(mQuery, M_CHARSET);
        } catch (Exception e) {
        }
        String searchUrl = "https://tw.search.buy.yahoo.com/search/shopping/product?p=" + queryStrEnc;

        try {
            exeTimer.start("yahoo download web");
            WebThread webThread = new WebThread(searchUrl, M_CHARSET);
            webThread.start();
            webThread.join();
            webThread.dumpToFile("debug_yahoo.html");
            Document doc = webThread.getDoc();
            exeTimer.stop();

            exeTimer.start("yahoo parse web ");
            Elements elements = doc.select("div.item.yui3-g");
            exeTimer.stop();
            Debugger.println("downloadSizeByte = " + webThread.htmlSize() / 1024 + " KB", "Yahoo");
            Debugger.println("elementsCount = " + elements.size(), "Yahoo");

            exeTimer.start("ruten match items");
            for (Element elem : elements) {

                String itemText = elem.select("div.srp-pdtitle.ellipsis").text();
                if(itemText.isEmpty()){
                    continue;
                }

                Elements elemPrice = elem.select("div.srp-pdprice");
                int itemPrice = Integer.parseInt(elemPrice.select("em").text().replace(",", ""));


                Elements elemLink = elem.select("div.srp-pdimage");
                String url = elemLink.select("a").attr("href");
                String imgUrl = elemLink.select("img").attr("src");

                Item item = new Item(itemText, itemPrice, url, Store.YAHOO, imgUrl);

                mItemSet.add(item);
                Debugger.println(item);
            }
            exeTimer.stop();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SThreadYahoo thd = new SThreadYahoo("zenfone 2");
        thd.run();

        try {
            thd.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
