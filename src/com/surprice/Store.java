package com.surprice;

public enum Store {NONE, RUTEN, RS, MOMO, COOLPC, LANDTOP, SUNFAR, YAHOO;
    @Override
    public String toString(){
        String str = "";
        switch (this){
            case RUTEN:
                str = "露天拍賣";
                break;
            case RS:
                str = "PChome";
                break;
            case MOMO:
                str = "MOMO";
                break;
            case COOLPC:
                str = "原價屋";
                break;
            case LANDTOP:
                str = "地標網通";
                break;
            case SUNFAR:
                str = "順發";
                break;
            case YAHOO:
                str = "順發";
                break;
            default:
                break;
        }
        return str;
    }

    public static Store toEnum(String str){
        Store s = NONE;
        if(str.equals("RUTEN")){
            s = RUTEN;
        }else if(str.equals("RS")){
            s = RS;
        }else if(str.equals("MOMO")){
            s = MOMO;
        }else if(str.equals("COOLPC")){
            s = COOLPC;
        }else if(str.equals("LANDTOP")){
            s = LANDTOP;
        }else if(str.equals("SUNFAR")){
            s = SUNFAR;
        }else if(str.equals("YAHOO")){
            s = SUNFAR;
        }

        return s;
    }
}
