package com.surprice;

public class WebParser {

    public static void main(String [] args){
        WebParserCoolpc webParserCoolpc = new WebParserCoolpc();
        WebParserLandtop webParserLandtop = new WebParserLandtop();
        webParserCoolpc.run();
        webParserLandtop.run();
        try{
            webParserCoolpc.join();
            webParserLandtop.join();
        }catch (Exception e){
            e.printStackTrace();
        }

        JsonWriter.output("coolpc", webParserCoolpc.getItemList());
        JsonWriter.output("landtop", webParserLandtop.getItemList());

        JsonParser.main(null);
    }
}
