package com.surprice;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.security.Key;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebParserCoolpc extends Thread {

    private ArrayList<Item> mItemList = new ArrayList<>();
    private ExeTimer exeTimer = new ExeTimer();
    private final String M_CHARSET = "Big5";
    private final String coolpcUrl = "http://www.coolpc.com.tw/evaluate.php";

    public List<Item> getItemList() {
        return mItemList;
    }

    @Override
    public void run() {
        parseWeb();
    }

    // search from coolpc website
    private void parseWeb() {
        try {
            exeTimer.start("coolpc download web");
            WebThread webThread = new WebThread(coolpcUrl, M_CHARSET);
            webThread.useCache(true);
            webThread.setCacheExpireTimeMs(60 * 60 * 1000L); // expiration time: 1 hour
            webThread.start();
            webThread.join();
            webThread.dumpToFile(Config.STORAGE_PATH + "debug_coolpc.html");
            Document doc = webThread.getDoc();
            exeTimer.stop();

            exeTimer.start("coolpc parse web");
            Elements elements = doc.select("OPTION");
            exeTimer.stop();
            Debugger.println("downloadSizeByte = " + webThread.htmlSize() / 1024 + " KB", "Coolpc");
            Debugger.println("elementsCount = " + elements.size(), "Coolpc");

            exeTimer.start("coolpc match items");
            int option_group_counter = 0;
            for (Element elem : elements) {
                int option_value = Integer.parseInt(elem.attr("value"));
                if (option_value == 0) {
                    option_group_counter++;
                    //System.out.println("Group" + option_group_counter);
                }

                // ignore group 26 / 30
                if (option_group_counter == 26 || option_group_counter == 30) {
                    continue;
                }

                int nameEnd = elem.text().indexOf(",");
                if (nameEnd == -1)
                    nameEnd = elem.text().length();
                String itemName = elem.text().substring(0, nameEnd);

                // filter out "送"
                int indexGi = itemName.indexOf("送");
                if (indexGi != -1) {
                    while (KeywordSplitter.charType(itemName.charAt(indexGi - 1)) == KeywordSplitter.CharType.SYM) {
                        indexGi -= 1;
                    }
                    itemName = itemName.substring(0, indexGi);
                }

                int itemPrice = 0;
                try {
                    String str = elem.text().toString();
                    int dollarSignPos = str.lastIndexOf("$");
                    int priceEndPos = str.indexOf(" ", dollarSignPos);
                    if(priceEndPos == -1) priceEndPos = str.length();
                    //System.out.print(str + " " + dollarSignPos + ": ");
                    if (dollarSignPos != -1) {
                        itemPrice = Integer.parseInt(str.substring(dollarSignPos + 1, priceEndPos));
                    }
                    //System.out.println(itemPrice);
                } catch (NumberFormatException ignored) {
                }

                String imgUrl = "http://www.coolpc.com.tw/phpBB2/styles/610nm/imageset/logos/newlogo0827.gif";
                Item item = new Item(itemName, itemPrice, "", Store.COOLPC, imgUrl);

                // ignore garbage data
                if (item.getPrice() <= 10) {
                    continue;
                }
                mItemList.add(item);
            }
            exeTimer.stop();
            Debugger.println("itemsCount = " + mItemList.size(), "Coolpc");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
