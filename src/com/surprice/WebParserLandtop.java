package com.surprice;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.*;

public class WebParserLandtop extends Thread {

    private List<Item> mItemList = new ArrayList<>();
    private ExeTimer exeTimer = new ExeTimer();
    private final String M_CHARSET = "UTF-8";
    private final String landtopUrl = "http://www.landtop.com.tw/products.php?types=1";

    public List<Item> getItemList() {
        return mItemList;
    }

    @Override
    public void run() {
        parseWeb();
    }

    // search from landtop website
    private void parseWeb() {
        try {
            exeTimer.start("landtop download web");
            WebThread webThread = new WebThread(landtopUrl, M_CHARSET);
            webThread.useCache(true);
            webThread.setCacheExpireTimeMs(60 * 60 * 1000L); // expiration time: 1 hour
            webThread.start();
            webThread.join();
            webThread.dumpToFile(Config.STORAGE_PATH + "debug_landtop.html");
            Document doc = webThread.getDoc();
            exeTimer.stop();

            exeTimer.start("landtop parse web");
            Elements elements = doc.select("tr");
            exeTimer.stop();
            Debugger.println("downloadSizeByte = " + webThread.htmlSize() / 1024 + " KB", "Landtop");
            Debugger.println("elementsCount = " + elements.size(), "Landtop");

            exeTimer.start("Landtop match items");
            for (Element elem : elements) {
                String item_text = elem.select("h5 > a").text();
                int item_price;
                try{
                    item_price  = Integer.parseInt(elem.select("td.price").text().replace("$ ",""));
                }catch (NumberFormatException e){
                    continue; // skip this item
                }
                String url = elem.select("a").attr("href");

                if(url.contains("idept=1&")){
                    item_text = "apple " + item_text;
                }else if(url.contains("idept=2&")){
                    item_text = "asus " + item_text;
                }else if(url.contains("idept=7&")){
                    item_text = "htc " + item_text;
                }else if(url.contains("idept=4&")){
                    item_text = "lg " + item_text;
                }else if(url.contains("idept=6&")){
                    item_text = "samsung " + item_text;
                }else if(url.contains("idept=5&")){
                    item_text = "sony " + item_text;
                }else if(url.contains("idept=9&")){
                    item_text = "infocus " + item_text;
                }else if(url.contains("idept=14&")){
                    item_text = "huawei " + item_text;
                }else if(url.contains("idept=13&")){
                    item_text = "xiaomi " + item_text;
                }else if(url.contains("idept=20&")){
                    item_text = "oppo " + item_text;
                }else if(url.contains("idept=16&")){
                    item_text = "benten " + item_text;
                }else if(url.contains("idept=17&")){
                    item_text = "nokia " + item_text;
                }else if(url.contains("idept=25&")){
                    item_text = "hugiga " + item_text;
                }

                String urlBase = "http://www.landtop.com.tw/";
                Item item = new Item(item_text, item_price, urlBase+url, Store.LANDTOP, urlBase+"images/logo.jpg");
                mItemList.add(item);
            }
            exeTimer.stop();
            Debugger.println("itemsCount = " + mItemList.size(), "Landtop");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
