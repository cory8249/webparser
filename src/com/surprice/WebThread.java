package com.surprice;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Date;

public class WebThread extends Thread {

    private String mUrl;
    private String mCacheFilename;
    private String mCharset;
    private Document mDoc;
    private long mCacheExpireTimeMs = 0L;
    private boolean mUseCache = false;
    private boolean mHasCache = false;
    private final String M_CLASSNAME = "WebThread";
    private final String M_USER_AGENT = "Chrome/45.0.2454.93";

    public WebThread(String url, String charset) {
        mUrl = url;
        mCharset = charset;
        mCacheFilename = "web_" + hashMD5(mUrl) + ".html";
    }

    @Override
    public void run() {
        try {
            File cacheFile = new File(Config.STORAGE_PATH + mCacheFilename);
            if(cacheFile.exists()){
                Date lastModDate = new Date(cacheFile.lastModified());
                Date curDate = new Date();
                long diffMs = curDate.getTime() - lastModDate.getTime();
                if(diffMs > mCacheExpireTimeMs){
                    Debugger.println("cache expired", M_CLASSNAME);
                    mHasCache = false; // cache expired, reload a new one from internet
                }else{
                    Debugger.println("cache still alive, remaining "
                            + String.valueOf((mCacheExpireTimeMs -diffMs)/1000L)
                            + " second", M_CLASSNAME);
                    mHasCache = true; // assume there is no error in the file.
                }
            }

            if(mHasCache && mUseCache){
                Debugger.println("read cached web " + mUrl + " file = " + mCacheFilename, M_CLASSNAME);
                mDoc = Jsoup.parse(cacheFile, mCharset);
            }else{
                Debugger.println("connect to " + mUrl, M_CLASSNAME);
                Debugger.printlnToFile("mUrl = " + mUrl);
                mDoc = Jsoup.connect(mUrl).timeout(5_000).userAgent(M_USER_AGENT).get();
                Debugger.printlnToFile("mDoc.location() = " + mDoc.location());
                if(mUseCache){
                    Debugger.println("save web to cache file " + mCacheFilename, M_CLASSNAME);
                    dumpToFile(Config.STORAGE_PATH + mCacheFilename);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public int htmlSize(){
        if(mDoc == null) return 0;
        return mDoc.html().length();
    }

    public Document getDoc(){
        return mDoc;
    }

    public void useCache(boolean useOrNot){
        mUseCache = useOrNot;
    }

    public void setCacheExpireTimeMs(long ms){
        mCacheExpireTimeMs = ms;
    }

    public void dumpToFile(String filename){
        if(mDoc==null || filename == null)
            return;
        try{
            File file = new File(filename);
            PrintWriter printWriter = new PrintWriter(file, mCharset);
            printWriter.println("input_url = " + mDoc.location() + "<br>");
            printWriter.println("real_url = " + mDoc.location()  + "<br>");
            printWriter.write(mDoc.html());
            printWriter.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private String hashMD5(String encTarget){
        try {
            MessageDigest mdEnc;
            mdEnc = MessageDigest.getInstance("MD5");
            mdEnc.update(encTarget.getBytes(), 0, encTarget.length());
            String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
            while ( md5.length() < 32 ) {
                md5 = "0"+md5;
            }
            return md5;

        } catch (Exception e) {
            Debugger.println("Exception while encrypting to md5", "hashMD5");
            e.printStackTrace();
        } // Encryption algorithm
        return "";
    }
}
